@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form action="{{ route('addCat') }}" method="POST">
                            @csrf
                            <div>
                                <label for="catName">Category Name :</label>
                                <input class="form-control" type="text" name="cat_name" id=""
                                    placeholder="Enter Category Name Here!">
                            </div>
                            <div>
                                <input type="submit" name="submit" id="" value="Add Category!" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
