@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>
                    @role('seller')
                    <div><a href="/addCat" class="btn btn-success">Add Category!</a></div>
                    @endrole
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <table class="table table-bordered">
                            <tr>
                                <th>Index</th>
                                <th>Category Name</th>
                                @role('seller')
                                <th>Status</th>
                                @endrole
                                <th>Action</th>
                            </tr>
                            @forelse ($items as $key=>$item )
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $item->cat_name }}</td>
                                    @role('seller')
                                    <td>
                                        {{ $item->status }}
                                    </td>
                                    @endrole
                                    <td>
                                        @role('seller')
                                        <a href="editCat/{{ $item->id }}" class="btn btn-primary">Edit!</a>
                                        <a href="deleteCat/{{ $item->id }}" class="btn btn-danger">Delete!</a>
                                        @endrole
                                        @role('admin')
                                        <a href="approveCat/{{ $item->id }}" class="btn btn-success">Approve</a>
                                        <a href="rejectCat/{{ $item->id }}" class="btn btn-danger">Reject</a>
                                        @endrole
                                    </td>
                                </tr>
                            @empty

                            @endforelse
                        </table>
                        @role('admin')
                        <p>This is admin!!!</p>
                        @endrole
                        {{ __('You are logged in!') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
