<?php

use App\Http\Controllers\CategoryController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::view('/addCat', 'addCat');

Route::post('/addCat', [CategoryController::class, 'catStore'])->name('addCat');

Route::get('/editCat/{id}', [CategoryController::class, 'editCat']);

Route::post('/editCat/{id}', [CategoryController::class, 'updateCat']);

Route::get('/approveCat/{id}', [CategoryController::class, 'approveCat']);

Route::get('/rejectCat/{id}', [CategoryController::class, 'rejectCat']);

Route::get('/deleteCat/{id}', [CategoryController::class, 'deleteCat']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\CategoryController::class, 'getCatData'])->name('home');
