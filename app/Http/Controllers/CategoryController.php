<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    public function catStore()
    {
        $user = Auth::user()->id;
        $catData = Categories::create([
            'seller_id' => $user,
            'cat_name' => request('cat_name'),
            'status' => 'Pending'
        ]);
        return redirect('/home');
    }

    public function getCatData()
    {
        $items = Categories::all();

        return view('/home', compact('items'));
    }

    public function editCat($id)
    {
        $items = Categories::find($id);

        return view('/addCat', compact('items'));
    }

    public function updateCat($id)
    {
        Categories::where('id', $id)->update([
            'cat_name' => request('cat_name'),
        ]);
        return redirect('/home');
    }

    public function rejectCat($id)
    {
        Categories::where('id', $id)->update([
            'status' => 'Reject',
        ]);

        return redirect('/home');
    }

    public function approveCat($id)
    {
        Categories::where('id', $id)->update([
            'status' => 'Approve',
        ]);

        return redirect('/home');
    }

    public function deleteCat($id)
    {
        $item = Categories::find($id)->delete();
        return redirect('/home');
    }
}
